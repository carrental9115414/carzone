import { Component } from '@angular/core';
import { NgForm } from '@angular/forms';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.component.html',
  styleUrls: ['./forgotpassword.component.css']
})
export class ForgotpasswordComponent {
  
  constructor(private http: HttpClient) { }

  submitForgotPassword(forgotPasswordForm: NgForm) {
    if (forgotPasswordForm.valid) {
      const email = forgotPasswordForm.value.email;
      const password = forgotPasswordForm.value.password;

      this.http.post<any>('http://8085/updatePassword', { email, password })
        .subscribe(
          response => {
            console.log(response);
          },
          error => {
            console.error(error);
          }
        );
    }
  }
}

