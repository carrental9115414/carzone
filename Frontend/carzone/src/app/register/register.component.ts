// import { Component } from '@angular/core';
// import { Router } from '@angular/router'; // Ensure Router is imported
// import { CustomerserviceService } from '../customerservice.service'; // Adjust the import path as necessary

// @Component({
//   selector: 'app-register',
//   templateUrl: './register.component.html',
//   styleUrls: ['./register.component.css']
// })
// export class RegisterComponent {
//   fullName: string = '';
//   email: string = '';
//   password: string = '';
//   phoneNumber: string = '';
//   licenseNumber: string = '';
//   address: string = '';
//   confirmPassword: string = ''; // Define it as a string rather than string | undefined for consistency

//   constructor(private customerService: CustomerserviceService, private router: Router) {} // Inject Router here

//   passwordsMatch(): boolean {
//     return this.password === this.confirmPassword;
//   }

//   customerRegister() {
//     if (this.passwordsMatch()) {
//       const customerData = {
//         fullName: this.fullName,
//         email: this.email,
//         password: this.password,
//         phoneNumber: this.phoneNumber,
//         licenseNumber: this.licenseNumber,
//         address: this.address
//       };

//       // Call the service to register the customer
//       this.customerService.addCustomer(customerData).subscribe({
//         next: (response: any) => {
//           console.log('Registration successful', response);
//           // Navigate to a different route, maybe login page after successful registration
//           this.router.navigate(['/login']); // Adjust the route as necessary
//         },
//         error: (error: any) => {
//           console.error('Registration failed', error);
//           // Show error message to the user
//         }
//       });
//     } else {
//       alert("Passwords do not match!");
//     }
//   }
// }
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { CustomerserviceService } from '../customerservice.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent {
  fullName: string = '';
  email: string = '';
  password: string = '';
  phoneNumber: string = '';
  licenseNumber: string = '';
  address: string = '';
  confirmPassword: string = '';
  userAnswer: string = ''; // User's answer to the CAPTCHA
  captchaQuestion!: string;
  correctAnswer!: number;

  constructor(private customerService: CustomerserviceService, private router: Router) {
    this.generateCaptcha();
  }

  generateCaptcha(): void {
    const a = Math.floor(Math.random() * 10) + 1;
    const b = Math.floor(Math.random() * 10) + 1;
    this.captchaQuestion = `${a} + ${b} = ?`;
    this.correctAnswer = a + b;
  }

  passwordsMatch(): boolean {
    return this.password === this.confirmPassword;
  }

  customerRegister() {
    if (!this.passwordsMatch()) {
      alert("Passwords do not match!");
      return;
    }

    if (parseInt(this.userAnswer) !== this.correctAnswer) {
      alert('Incorrect CAPTCHA. Please try again.');
      this.generateCaptcha(); // Generate a new CAPTCHA
      return;
    }

    const customerData = {
      fullName: this.fullName,
      email: this.email,
      password: this.password,
      phoneNumber: this.phoneNumber,
      licenseNumber: this.licenseNumber,
      address: this.address
    };

    // Call the service to register the customer
    this.customerService.addCustomer(customerData).subscribe({
      next: (response: any) => {
        console.log('Registration successful', response);
        this.router.navigate(['/login']); // Adjust the route as necessary
      },
      error: (error: any) => {
        console.error('Registration failed', error);
      }
    });
  }
}

