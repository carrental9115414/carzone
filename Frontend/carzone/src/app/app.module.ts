import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatCardModule } from '@angular/material/card';
import { AppRoutingModule } from './app-routing.module';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { ToastrModule } from 'ngx-toastr';
import { CarouselModule } from 'ngx-bootstrap/carousel';

// Components
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { RegisterComponent } from './register/register.component';
import { HomeComponent } from './home/home.component';
import { CarsComponent } from './cars/cars.component';
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { ContactUsComponent } from './contactus/contactus.component';
import { FooterComponent } from './footer/footer.component';
import { PopulardestinationsComponent } from './populardestinations/populardestinations.component';
import { SpecialoffersComponent } from './specialoffers/specialoffers.component';
import { WhychooseusComponent } from './whychooseus/whychooseus.component';
import { TestimonialsComponent } from './testimonials/testimonials.component';
import { CardComponent } from './card/card.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LoginComponent,
    LogoutComponent,
    RegisterComponent,
    HomeComponent,
    CarsComponent,
    ForgotpasswordComponent,
    AboutusComponent,
    ContactUsComponent,
    FooterComponent,
    PopulardestinationsComponent,
    SpecialoffersComponent,
    WhychooseusComponent,
    TestimonialsComponent,
    CardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    MatSnackBarModule,
    BrowserAnimationsModule, // Ensure animations work correctly
    ToastrModule.forRoot(), // Initialize Toastr
    CarouselModule.forRoot(), // Initialize Carousel
    RouterModule,
    MatCardModule // Material Card Module for card components
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
