import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';

interface Destination {
  title: string;
  description: string;
  imgSrc: string;
}

@Component({
  selector: 'app-populardestinations',
  templateUrl: './populardestinations.component.html',
  styleUrls: ['./populardestinations.component.css']
})
export class PopulardestinationsComponent implements AfterViewInit {
  @ViewChild('scrollContainer')
  scrollContainer!: ElementRef<HTMLDivElement>;

  destinations: Destination[] = [
    {
      title: "Delhi",
      description: "Explore the bustling streets of NYC!",
      imgSrc: "assets/delhi.jpeg"
    },
    {
      title: "Kolkata",
      description: "Witness the breathtaking views of the Grand Canyon.",
      imgSrc: "assets/kolkata.jpeg"
    },
    {
      title: "Hyderabad",
      description: "Enjoy the romantic vibes of the city of love!",
      imgSrc: "assets/hyd.jpeg"
    },
    {
      title: "Varanasi",
      description: "Dive into the vibrant culture and technology of Tokyo.",
      imgSrc: "assets/ayodhya.jpeg"
    },
    {
      title: "Kerla",
      description: "Experience the underwater paradise in Australia.",

      imgSrc: "assets/th.jpeg"

    },
    {
      title: "Chennai",
      description: "Discover the rich history and modern charm of London.",
      imgSrc: "assets/chennai.jpeg"
    },
    {
      title: "Mumbai",
      description: "Relax in the stunning beauty of Greece's island paradise.",

      imgSrc: "assets/th.jpeg"

    }
    // Add more destinations as needed
  ];

  constructor() {}

  ngAfterViewInit(): void {
    this.startAutoScroll();
  }

  startAutoScroll() {
    const speed = 1; // Adjust speed as needed
    const scroll = () => {
      this.scrollContainer.nativeElement.scrollLeft += speed;
      requestAnimationFrame(scroll);
    };
    scroll();
  }
}
