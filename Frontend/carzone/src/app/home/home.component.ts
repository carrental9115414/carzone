import { Component, OnInit, ViewChild, ElementRef, AfterViewInit ,HostListener} from '@angular/core';
import { trigger, state, style, transition, animate } from '@angular/animations';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  animations: [
    trigger('fadeIn', [
      state('void', style({ opacity: 0 })),
      transition('void => *', [animate('1s ease-in')]),
    ])
  ]
})
export class HomeComponent implements OnInit, AfterViewInit {
  @ViewChild('videoPlayer')
  videoPlayer!: ElementRef<HTMLVideoElement>;

  offers = [
    {
      title: 'Summer Special',
      description: 'Get 20% off on your summer holiday rentals!',
      imageUrl: 'assets/image3.jpeg' 
    },
    {
      title: 'Weekend Getaways',
      description: 'Special weekend rates starting from $19.99!',
      imageUrl: 'assets/image4.jpeg'
    },
    // Add more offers as needed
  ];

  cards = [
    {
      title: 'Small-Sized',
      description: '"Discover compact, fuel-efficient cars ideal for easy city travel in our rental service."',
      imgSrc: 'assets/car11.jpeg' // Example image path, adjust as needed
    },
    {
      title: 'Medieum-Sized',
      description: '"Enjoy enhanced comfort and performance with our mid-range cars, designed for superior journeys."',
      imgSrc: 'assets/car2.jpeg' // Example image path, adjust as needed
    },
    {
      title: 'Highlevel',
      description: '"Experience luxury and space with our high-end, large vehicles for supreme comfort and group travels."',
      imgSrc: 'assets/car3.jpeg' // Example image path, adjust as needed

      

    }
    // Add more cards as needed
  ];
  
  constructor() { }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    this.videoPlayer.nativeElement.muted = true;
  }
}
