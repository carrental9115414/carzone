
// In your card.component.ts

import { Component, Input } from '@angular/core';



@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',

  styleUrls: ['./card.component.css']
})
export class CardComponent {
  @Input() data: any;

  styleUrl: './card.component.css' = "./card.component.css";
}
