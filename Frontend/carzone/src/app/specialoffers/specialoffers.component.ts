import { Component, OnInit } from '@angular/core';

interface Offer {
  title: string;
  description: string;
  imgSrc: string;
  isBestOffer?: boolean; // New property to flag the best offer
}

@Component({
  selector: 'app-specialoffers',
  templateUrl: './specialoffers.component.html',
  styleUrls: ['./specialoffers.component.css']
})
export class SpecialoffersComponent implements OnInit {
  offers: Offer[] = [
    {
      title: "Summer Special",
      description: "Get 20% off on all sedan rentals in July!",

      imgSrc: "assets/summer.jpeg"

    },
    {
      title: "Weekend Getaway Deals",
      description: "Book a car for the weekend and get a third day free!",

      imgSrc: "assets/weekend.jpeg"

    },
    // Adding a best special offer
    {
      title: "Exclusive Luxury Package",
      description: "Experience unmatched luxury at half the price. Limited time offer!",
      imgSrc: "assets/luxzary.jpeg",
      isBestOffer: true // Marking this as the best special offer
    }
    // Add more offers as needed
  ];

  constructor() {}

  ngOnInit(): void {}
}
