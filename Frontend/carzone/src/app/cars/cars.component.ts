import { Component, OnInit } from '@angular/core';
import { CustomerserviceService } from '../customerservice.service'; 

@Component({
  selector: 'app-cars',
  templateUrl: './cars.component.html',
  styleUrls: ['./cars.component.css']
})
export class CarsComponent implements OnInit {
  cars: any;

  constructor(public service: CustomerserviceService) {}

  ngOnInit() {
    this.service.getAllCars().subscribe((data: any) => {
      console.log('Cars:', data); 
      this.cars = data;
    });
  }
}
