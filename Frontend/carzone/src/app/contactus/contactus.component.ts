// import { Component, OnInit } from '@angular/core';
// import { HttpClient } from '@angular/common/http';

// interface ContactForm {
//   name: string;
//   email: string;
//   message: string;
// }

// @Component({
//   selector: 'app-contactus',
//   templateUrl: './contactus.component.html',
//   styleUrls: ['./contactus.component.css']
// })
// export class ContactUsComponent implements OnInit { // Correct casing of the class name
//   formData: ContactForm = { name: '', email: '', message: '' };
//   successMessage: string = '';

//   constructor(private http: HttpClient) { }

//   ngOnInit(): void {
//   }

//   submitForm(): void {
//     this.http.post<any>('http://localhost:8080/api/contact/submit', this.formData)
//       .subscribe(response => {
//         console.log(response);
//         this.successMessage = 'Form submitted successfully!';
//         this.formData = { name: '', email: '', message: '' }; // Clear form fields
//       }, error => {
//         console.error('Error:', error);
//         this.successMessage = 'Error submitting form. Please try again later.';
//       });
//   }
// }
import { Component, OnInit } from '@angular/core';
import { CustomerserviceService } from '../customerservice.service'; // Update path as needed

@Component({
  selector: 'app-contactus',
  templateUrl: './contactus.component.html',
  styleUrls: ['./contactus.component.css']
})
export class ContactUsComponent implements OnInit {
  formData: any = {}; // Declare formData as any type
  successMessage: string = '';
  errorMessage: string = '';

  constructor(private customerService: CustomerserviceService) { }

  ngOnInit(): void {
  }

  submitForm(): void {
    this.customerService.addCustomer(this.formData)
      .subscribe((response: any) => {
        console.log(response);
        this.successMessage = 'Form submitted successfully!';
        this.errorMessage = ''; // Clear any previous error message
        this.formData = {}; // Clear form fields
      }, (error: any) => {
        console.error('Error:', error);
        this.successMessage = ''; // Clear any previous success message
        this.errorMessage = 'Error submitting form. Please try again later.';
      });
  }
}
