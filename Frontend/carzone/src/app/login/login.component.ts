// import { Component } from '@angular/core';
// import { Router } from '@angular/router';
// import { CustomerserviceService } from '../customerservice.service';
// import { AuthService } from '../auth.service';

// @Component({
//   selector: 'app-login',
//   templateUrl: './login.component.html',
//   styleUrls: ['./login.component.css']
// })
// export class LoginComponent {
//   email: string = '';
//   password: string = '';
//   customer: any = null;

//   constructor(
//     private router: Router,
//     private service: CustomerserviceService,
//     private authService: AuthService
//   ) {}

//   async loginSubmit(loginForm: any) {
//     localStorage.setItem('email', loginForm.email);

//     if (loginForm.email == 'Admin' && loginForm.password == 'Admin') {
//       this.authService.login();
//       this.router.navigate(['home']);
//     } else {
//       try {
//         const data = await this.service.customerLogin(loginForm.email, loginForm.password).toPromise();
//         if (data) {
//           this.authService.login();
//           this.router.navigate(['cars']);
//         } else {
//           alert('Invalid Credentials');
//           this.router.navigate(['login']);
//         }
//       } catch (error) {
//         console.error(error);
//         alert('Login failed. Please try again.');
//       }
//     }
//   }
// }
// import { Component } from '@angular/core';
// import { Router } from '@angular/router';
// import { ToastrService } from 'ngx-toastr';
// import { CustomerserviceService } from '../customerservice.service';
// import { AuthService } from '../auth.service';

// @Component({
//   selector: 'app-login',
//   templateUrl: './login.component.html',
//   styleUrls: ['./login.component.css']
// })
// export class LoginComponent {
//   email: string = '';
//   password: string = '';
//   customer: any = null;

//   constructor(
//     private router: Router,
//     private service: CustomerserviceService,
//     private authService: AuthService,
//     private toastr: ToastrService // Inject ToastrService
//   ) {}

//   async loginSubmit(loginForm: any) {
//     localStorage.setItem('email', loginForm.email);

//     if (loginForm.email == 'Admin' && loginForm.password == 'Admin') {
//       this.authService.login();
//       this.toastr.success('Login Successful', 'Welcome Admin!'); // Toastr success message
//       this.router.navigate(['home']);
//     } else {
//       try {
//         const data = await this.service.customerLogin(loginForm.email, loginForm.password).toPromise();
//         if (data) {
//           this.authService.login();
//           this.toastr.success('Login Successful', 'Welcome!'); // Toastr success message
//           this.router.navigate(['cars']);
//         } else {
//           this.toastr.error('Invalid Credentials', 'Login Failed'); // Toastr error message
//           this.router.navigate(['login']);
//         }
//       } catch (error) {
//         console.error(error);
//         this.toastr.error('Login failed. Please try again.', 'Error'); // Toastr error message
//       }
//     }
//   }
// }
import { Component } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service'; 
import { CustomerserviceService } from '../customerservice.service'; 

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {
  email: string = '';
  password: string = '';

  constructor(
    private customerService: CustomerserviceService,
    private router: Router,
    private snackBar: MatSnackBar
  ) {}

  loginSubmit(loginForm: any) {
    this.customerService.customerLogin(loginForm.email, loginForm.password)
      .subscribe({
        next: (response) => {
          this.customerService.isUserLoggedIn();
          this.snackBar.open('Login Successful!', 'Close', { duration: 3000 });
          this.router.navigate(['home']); 
        },
        error: (error) => {
          console.error(error);
          this.snackBar.open('Login Failed. Please try again.', 'Close', { duration: 3000 });
        }
      });
  }
}
