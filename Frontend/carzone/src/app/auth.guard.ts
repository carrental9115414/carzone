import { CanActivateFn } from '@angular/router';
import { inject } from '@angular/core';
import { CustomerserviceService } from './customerservice.service';

export const authGuard: CanActivateFn = (route, state) => {
  
  let service = inject(CustomerserviceService);

  return service.isLoggedIn();
};

