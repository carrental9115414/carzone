import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CustomerserviceService {
  private baseUrl = 'http://localhost:8085';

  private loggedInStatus = JSON.parse(localStorage.getItem('loggedIn') || 'false');

  constructor(private http: HttpClient) { }

  isUserLoggedIn() {
    localStorage.setItem('loggedIn', 'true');
    this.loggedInStatus = true;
  }

  isLoggedIn() {
    return JSON.parse(localStorage.getItem('loggedIn') || this.loggedInStatus.toString());
  }

  logoutUser() {
    localStorage.removeItem('loggedIn');
    this.loggedInStatus = false;
  }

  customerLogin(email: string, password: string): Observable<any> {
    const body = { email: email, password: password };
    return this.http.post<any>(`${this.baseUrl}/loginCustomer`, body);
  }

  
  addCustomer(customerData: { fullName: string; email: string; password: string; phoneNumber: string; licenseNumber: string; address: string; }): Observable<any> {
    return this.http.post<any>(`${this.baseUrl}/addCustomer`, customerData);
  }

  getLoginStatus(): boolean {
    return this.loggedInStatus;
  }

  getAllCars(): Observable<any> {
    return this.http.get(`${this.baseUrl}/cars`);
  }
}
