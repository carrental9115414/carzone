import { Component } from '@angular/core';


interface Testimonial {
  message: string;
  author: string;
}

@Component({
  selector: 'app-testimonials',
  templateUrl: './testimonials.component.html',
  styleUrls: ['./testimonials.component.css']
})
export class TestimonialsComponent {
  testimonials: Testimonial[] = [
    {
      message: "The best car rental experience I've ever had! Easy booking process and excellent customer service.",
      author: "Jane Doe"
    },
    {
      message: "Impressed with the wide range of cars available. The vehicle was clean and in perfect condition.",
      author: "John Smith"
    },
    // Add more testimonials as needed
  ];

  // Track which testimonial is currently being shown
  currentTestimonialIndex = 0;

  // Function to go to the next testimonial
  nextTestimonial(): void {
    this.currentTestimonialIndex = (this.currentTestimonialIndex + 1) % this.testimonials.length;
  }



}
