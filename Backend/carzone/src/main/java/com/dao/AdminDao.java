package com.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.model.Admin;

@Service
public class AdminDao {

    @Autowired
    private AdminRepository adminRepository;

    public Admin getAdminByEmail(String email) {
        return adminRepository.findByEmail(email).orElse(null);
    }

	
  
}
