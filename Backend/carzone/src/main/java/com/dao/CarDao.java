	package com.dao;
	
	import java.util.List;
	import org.springframework.beans.factory.annotation.Autowired;
	import org.springframework.stereotype.Service;
	import com.model.Car;
	import  com.dao.CarRepository;
	
	@Service
	public class CarDao {
	
	    @Autowired
	    private CarRepository carRepository;
	
	    public List<Car> getAllCars() {
	        return carRepository.findAll();
	    }
	
	    public Car getCarById(Long carId) {
	        return carRepository.findById(carId).orElse(null);
	    }
	
	    public List<Car> getCarsByCompanyName(String companyName) {
	        return carRepository.findByCompanyName(companyName);
	    }
	
	    public Car addCar(Car car) {
	        return carRepository.save(car);
	    }
	
	    public Car updateCar(Car car) {
	        return carRepository.save(car);
	    }
	
	    public void deleteCarById(Long carId) {
	        carRepository.deleteById(carId);
	    }
	    
	    
	}
