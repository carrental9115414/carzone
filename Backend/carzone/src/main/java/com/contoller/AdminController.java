package com.contoller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.dao.AdminDao;
import com.model.Admin;

@RestController
public class AdminController {

	@Autowired
	AdminDao adminDao;
	
	@GetMapping("getAdminByEmail/{id}")
	public Admin getProductById(@PathVariable("email") String email) {
		Admin admin = adminDao.getAdminByEmail(email);
		return admin;
	}
	
}
