package com.contoller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.dao.CustomerDao;

import com.model.Customer;


@CrossOrigin(origins = "http://localhost:4200") // Allows CORS requests from Angular app
@RestController
public class CustomerController {

    @Autowired
    private CustomerDao customerDao;
   
    

    @GetMapping("/getCustomerById/{id}")
    public ResponseEntity<Customer> getCustomerById(@PathVariable("id") Long id) {
        Customer customer = customerDao.getCustomerById(id);
        if (customer != null) {
            return ResponseEntity.ok(customer);
        } else {
            return ResponseEntity.notFound().build();
        }
    }
    @GetMapping("/getAllCustomers")
    public ResponseEntity<List<Customer>> getAllCustomers() {
        List<Customer> customers = customerDao.getAllCustomers();
        if (customers != null && !customers.isEmpty()) {
            return ResponseEntity.ok(customers);
        } else {
            return ResponseEntity.noContent().build(); 
        }
    }
    @PostMapping("/addCustomer")
    public ResponseEntity<Customer> addCustomer(@RequestBody Customer customer) {
        Customer savedCustomer = customerDao.addCustomer(customer);
        return ResponseEntity.ok(savedCustomer);
    }
    
    @PostMapping("/loginCustomer")
    public ResponseEntity<List<Customer>> loginCustomer(@RequestBody Map<String, String> credentials) {
        String email = credentials.get("email");
        String password = credentials.get("password");
        
        List<Customer> loggedInCustomer = customerDao.customerLogin(email, password);

        if (!loggedInCustomer.isEmpty()) {
            return ResponseEntity.ok(loggedInCustomer);
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }
    
    @PostMapping("/updatePassword")
    public ResponseEntity<String> updatePassword(@RequestBody Map<String, String> passwordData) {
        String email = passwordData.get("email");
        String newPassword = passwordData.get("newPassword");

        boolean passwordUpdated = customerDao.updatePasswordByEmail(email, newPassword);

        if (passwordUpdated) {
            return ResponseEntity.ok("Password updated successfully");
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Customer with email " + email + " not found");
        }
    }

    
    
}
